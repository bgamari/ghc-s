module Core where

import Control.Applicative

import qualified Data.Text.Encoding as TE
import qualified Data.Text as T
import qualified Data.ByteString.Char8 as C8

import GHC.S.Types
import GHC.S.FromS
import GHC.Core
import GHC.Core.Make
import GHC.Types.Literal
import GHC.Types.Var
import GHC.Utils.Outputable
import GHC.Driver.Session
import GHC.Types.Basic
import GHC.Types.Name
import GHC.Types.Id
import GHC.Types.Id.Info
import GHC.Core.TyCo.Rep
import GHC.Builtin.Types
import GHC.Types.SrcLoc

printCoreExpr :: CoreExpr -> IO ()
printCoreExpr expr = putStrLn $ showSDocUnsafe $ ppr expr

dbgCoreExpr :: S -> IO ()
dbgCoreExpr s =
  let p = runP (sToCoreExpr s) loc
  in (go <$> p) >>= printCoreExpr
  where
    go (Left err) = error $ show err
    go (Right a) = a

coreExprToS :: CoreExpr -> S
coreExprToS (Var v) = SAtom (getNameAtom v)
coreExprToS (Lit l) = getLiteral l
coreExprToS (Lam bdr expr) =
    STuple [SAtom (Atom "lam"), SAtom (getNameAtom bdr), coreExprToS expr]
coreExprToS (App fun arg) =
    STuple [SAtom (Atom "app"), coreExprToS fun, coreExprToS arg]
coreExprToS (Let (NonRec bdr rhs) body) =
    STuple [SAtom (Atom "let"), SAtom (getNameAtom bdr), coreExprToS rhs, coreExprToS body]
coreExprToS (Let (Rec binds) body) =
    STuple [ SAtom (Atom "letrec")
           , STuple (map (\(cBndr,cExpr) -> STuple [SAtom (getNameAtom cBndr), coreExprToS cExpr]) binds)
           , coreExprToS body
           ]
coreExprToS (Case scr altvar _ as) =
    STuple [ SAtom (Atom "case")
           , SAtom (getNameAtom altvar)
           , coreExprToS scr
           , STuple (convertAlt <$> as)
           ]
convertAlt :: Alt CoreBndr -> S
convertAlt (Alt DEFAULT _ rhs) =
    STuple [SAtom (Atom "_"), coreExprToS rhs]
convertAlt (Alt (LitAlt literal) _ rhs) =
    STuple [getLiteral literal, coreExprToS rhs]

getLiteral :: Literal -> S
getLiteral (LitNumber _ n) = SInt n
getLiteral (LitString s)  = SString (T.unpack (TE.decodeUtf8 s))

getNameAtom :: NamedThing n => n -> Atom
getNameAtom = Atom . getOccString

coreToS :: CoreProgram -> SRepr
coreToS ((NonRec b body):ps) = NonRecBind (getNameAtom b) (coreExprToS body) : coreToS ps
coreToS ((Rec binds):ps) = RecBind (map convertBind binds) : coreToS ps
  where
    convertBind (b, body) = (getNameAtom b, coreExprToS body)

mkVarAtom :: Atom -> Id
mkVarAtom (Atom v) = mkLocalVar VanillaId (simpleMkName v) anyTy anyTy vanillaIdInfo

sToAltCon :: S -> AltCon
sToAltCon (SInt i) = LitAlt (mkLitIntUnchecked i)
sToAltCon (SAtom (Atom "_")) = DEFAULT
sToAltCon _ = error "Invalid pattern"

-- (STuple [pat, expr])
mkAlt :: MonadFail m => S -> P m (Alt CoreBndr)
mkAlt  = expectTuple "alternative pattern" $ \xs -> do
    [pat, expr] <- pure xs
    pat' <- pure (sToAltCon pat)
    expr' <- sToCoreExpr expr
    return (Alt pat' [] expr')

mkBindings :: MonadFail m => S -> P m (Id, CoreExpr)
mkBindings = expectTuple "bindings" $ \xs -> do
    [SAtom atom, expr] <- pure xs
    expr' <- sToCoreExpr expr
    return (mkVarAtom atom,expr')

slet :: MonadFail m => S -> P m CoreExpr
slet = expectTuple "let expression" $ \xs -> do
    [SAtom (Atom "let"), SAtom atom, rhs, body] <- pure xs
    rhs' <- sToCoreExpr rhs
    body' <- sToCoreExpr body
    return $ mkLetNonRec (mkVarAtom atom) rhs' body'

slam :: MonadFail m => S -> P m CoreExpr
slam = expectTuple "lambda expression" $ \xs -> do
    [SAtom (Atom "lam"), SAtom atom, body] <- pure xs
    body' <- sToCoreExpr body
    return $ mkLams [mkVarAtom atom] body'

sletrec :: MonadFail m => S -> P m CoreExpr
sletrec = expectTuple "letrec expression" $ \xs -> do
    [SAtom (Atom "letrec"), STuple bindings, body] <- pure xs
    bindings' <- mapM mkBindings bindings
    body' <- sToCoreExpr body
    return $ mkLetRec bindings' body'

scase :: MonadFail m => S -> P m CoreExpr
scase = expectTuple "case" $ \xs -> do
  [SAtom (Atom "case"), scrutinee, SAtom bndr, alts] <- pure xs
  scrt' <- sToCoreExpr scrutinee
  alts' <- expectTuple "alternative cases" (mapM mkAlt) alts
  return $ Case scrt' (mkVarAtom bndr) (exprToType scrt') alts'

styvar :: MonadFail m => S -> P m CoreExpr
styvar = expectTuple "type variable" $ \xs -> do
  [SAtom (Atom "type"), SAtom var] <- pure xs
  return $ Type (TyVarTy (mkVarAtom var))

sapp :: MonadFail m => S -> P m CoreExpr
sapp = expectTuple "application" $ \xs -> do
  [fun, arg] <- pure xs
  fun' <- sToCoreExpr fun
  arg' <- sToCoreExpr arg
  return $ App fun' arg'

sbinop :: MonadFail m => S -> P m CoreExpr
sbinop = expectTuple "binary operation" $ \xs -> do
  [SAtom op, arg1, arg2] <- pure xs
  arg1' <- sToCoreExpr arg1
  arg2' <- sToCoreExpr arg2
  return $ App (App (Var (mkVarAtom op)) arg1') arg2'

sinteger :: MonadFail m => S -> P m CoreExpr
sinteger (SInt n) = pure $ mkUncheckedIntExpr n
sinteger _ = expected "expected integer"

sstring :: MonadFail m => S -> P m CoreExpr
sstring (SString str) = pure $ Lit (LitString (C8.pack str))
sstring _ = expected "expected string"

satom :: MonadFail m => S -> P m CoreExpr
satom (SAtom atom) = pure $ Var (mkVarAtom atom)
satom _ = expected "expected atom"

sToCoreExpr :: MonadFail m => S -> P m CoreExpr
sToCoreExpr s = sinteger s
              <|> sstring s
              <|> satom s
              <|> scase s
              <|> slam s
              <|> slet s
              <|> sletrec s
              <|> styvar s
              <|> sapp s
              <|> sbinop s

{-

f = \x -> case x of
            42 -> x+8
            _  -> x

~>

(letrec
  (f (lam x
    (case x
      (42 (app x 8))
      (wild (x))
    )
  )
)
-}
