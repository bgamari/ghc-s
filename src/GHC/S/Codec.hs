{-# LANGUAGE ScopedTypeVariables #-}

module GHC.S.Codec
  ( Codec(..)
  , integral
  , many
  ) where

import Control.Monad

import GHC.S.Types
import GHC.S.FromS (P)
import qualified GHC.S.FromS as FromS
import qualified GHC.S.ToS as ToS

data Codec m a = Codec { toS :: a -> S, fromS' :: S -> m a }

integral :: forall a m. (Bounded a, Integral a, MonadFail m)
         => Codec (P m) a
integral = Codec ToS.integral FromS.integral

many :: forall a m. MonadFail m => Codec (P m) a -> Codec (P m) [a]
many el = Codec (STuple . map (toS el)) (FromS.tuple >=> mapM (fromS el))

