{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}

module GHC.S.FromS
  ( FromS(..)
  , ParseError(..)
  , Loc(..)
  , loc
  , P
  , runP
    -- * Combinators
  , expectTuple
  , expected
    -- * Parsers
  , integer
  , string
  , tuple
  , atomp
  , theAtom
  , integral
  , simpleMkName
  ) where

import GHC.S.Types

import Data.Proxy

import GHC.Core.TyCo.Rep
import GHC.Core.TyCon
import GHC.Core.DataCon

import GHC.TypeLits
import GHC.Types.Basic
import GHC.Types.Cpr
import GHC.Types.Demand
import GHC.Types.Name
import GHC.Types.Unique
import GHC.Types.Var
import GHC.Types.Var.Env
import GHC.Types.Unique.FM
import GHC.Types.SourceText

import Data.Word
import Data.Int

import Data.Monoid

import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Reader

import Control.Monad
import Control.Applicative

data Loc = Loc deriving Show

loc :: Loc
loc = Loc

instance Semigroup Loc where
  _ <> _ = Loc

instance Monoid Loc where
  mempty = Loc

instance Semigroup ParseError where
  (ParseError _ p1) <> (ParseError _ p2) = (ParseError Loc (p1++p2))

instance Monoid ParseError where
  mempty = ParseError Loc ""

simpleMkName :: String -> Name
simpleMkName str =
    let uq = mkUnique 's' 7777
    in mkSystemName uq (mkVarOcc str)

data ParseError = ParseError Loc String deriving Show

newtype P m a = P (ExceptT ParseError (ReaderT Loc m) a)
  deriving (Functor, Applicative, Monad, MonadPlus, Alternative)

runP :: P m a -> Loc -> m (Either ParseError a)
runP (P p) loc = runReaderT (runExceptT p) loc

instance Monad m => MonadFail (P m) where
  fail msg = P $ do
    loc <- lift ask
    throwE $ ParseError loc msg

class FromS a where
  fromS :: MonadFail m => S -> P m a

expectTuple :: Monad m => String -> ([S] -> P m a) -> (S -> P m a)
expectTuple _ k (STuple xs) = k xs
expectTuple what _ _ = fail $ "expected tuple " ++ what

expected :: MonadFail m => String -> P m a
expected what =
  fail $ "expected " ++ what

parseBool :: MonadFail m => S -> P m Bool
parseBool (SAtom (Atom s)) = case s of
    "true"  -> return True
    "false" -> return False
    _       -> fail "Invalid boolean"
parseBool _ = fail "Expected an atom"

integral :: forall a m. (Bounded a, Integral a, MonadFail m)
         => S -> P m a
integral (SInt n)
  | n > fromIntegral (minBound @a)
  , n < fromIntegral (maxBound @a)
  = return $ fromInteger n
  | otherwise
  = fail "out-of-bounds"
integral _ = expected "integer"

integer :: forall m. (MonadFail m)
        => S -> P m Integer
integer (SInt n) = pure n
integer _ = expected "integer"

string :: MonadFail m => S -> P m String
string (SString s) = pure s
string _ = expected "string"

atomp :: MonadFail m => S -> P m Atom
atomp (SAtom s) = pure s
atomp _ = expected "atom"

tuple :: MonadFail m => S -> P m [S]
tuple (STuple xs) = pure xs
tuple _ = expected "tuple"

theAtom :: forall s m. (KnownSymbol s, MonadFail m)
        => S -> P m (TheAtom s)
theAtom s
  | SAtom (Atom a) <- s
  , a == sym  = return TheAtom
  | otherwise = expected $ "symbol " ++ show sym
  where sym = symbolVal (Proxy @s)

instance KnownSymbol s => FromS (TheAtom s) where fromS = theAtom @s

instance FromS Integer where fromS = integer

instance FromS Int where fromS = integral
instance FromS Int8 where fromS = integral
instance FromS Int16 where fromS = integral
instance FromS Int32 where fromS = integral
instance FromS Int64 where fromS = integral
instance FromS Word where fromS = integral
instance FromS Word8 where fromS = integral
instance FromS Word16 where fromS = integral
instance FromS Word32 where fromS = integral
instance FromS Word64 where fromS = integral

instance FromS a => FromS [a] where
    fromS s = flip (expectTuple "list occurence") s $ \xs -> mapM fromS xs

instance (FromS a, FromS b) => FromS (a,b) where
    fromS (STuple [a,b]) = (,) <$> fromS a <*> fromS b
    fromS _ = expected "2-tuple"

instance (FromS a, FromS b, FromS c) => FromS (a,b,c) where
    fromS (STuple [a,b,c]) = (,,) <$> fromS a <*> fromS b <*> fromS c
    fromS _ = expected "3-tuple"

instance (FromS a, FromS b, FromS c, FromS d) => FromS (a,b,c,d) where
    fromS (STuple [a,b,c,d]) = (,,,) <$> fromS a <*> fromS b <*> fromS c <*> fromS d
    fromS _ = expected "4-tuple"

instance FromS TailCallInfo where
    fromS tailCallInfoExpr =
            case tailCallInfoExpr of
                 SInt joinarity -> return (AlwaysTailCalled (fromInteger joinarity))
                 SAtom (Atom "no-tail") -> return NoTailCallInfo
                 _ -> fail "Invalid tailCallInfoExpr"

instance FromS InsideLam where
    fromS (SAtom (Atom il)) = case il of
            "inside-lam" -> return IsInsideLam
            "not-inside-lam" -> return NotInsideLam
            _ -> fail "Invalid InsideLam"
    fromS _ = fail "Needs an atom"

instance FromS InterestingCxt where
    fromS (SAtom (Atom ctx)) = case ctx of
            "interesting" -> return IsInteresting
            "not-interesting" -> return NotInteresting
            _ -> fail "Invalid InterestingCxt"
    fromS _ = fail "Needs an atom"

instance FromS Bool where
    fromS = parseBool

instance FromS OccInfo where
    fromS s = flip (expectTuple "occurence information") s $ \xs -> do
        [SAtom (Atom "occ-info"), rest] <- pure xs
        case rest of
            (SAtom (Atom "dead")) -> return IAmDead
            _ -> smanyocc s <|> soneocc s <|> sloopbreaker s
        where
            smanyocc = expectTuple "many occurence" $ \xs -> do
                [SAtom (Atom "many"), tailCallInfoExpr] <- pure xs
                tailCall <- fromS tailCallInfoExpr
                return (ManyOccs tailCall)

            soneocc = expectTuple "one occurrence" $ \xs -> do
                [SAtom (Atom "once"), inside, ctx, SInt brCount, tailCallInfoExpr] <- pure xs
                insideLam <- fromS inside
                interestingCxt <- fromS ctx
                tailCall <- fromS tailCallInfoExpr
                return (OneOcc insideLam (fromInteger brCount) interestingCxt tailCall)

            sloopbreaker = expectTuple "loop breaker" $ \xs -> do
                [SAtom (Atom "loop-breaker"), rules_only, tailCallInfoExpr] <- pure xs
                tailCall <- fromS tailCallInfoExpr
                ro <- fromS rules_only
                return (IAmALoopBreaker ro tailCall)

instance FromS SubDemand where
  fromS s = flip (expectTuple "SubDemand info") s $ \xs -> do
    [SAtom (Atom subType), rest] <- pure xs
    case subType of
      "poly" -> flip (expectTuple "poly info") rest $ \rs -> do
        [boxity, cardNonOnce] <- pure rs
        b <- fromS boxity
        c <- fromS cardNonOnce
        return $ Poly b c

      -- call is not exposed

      "prod" -> flip (expectTuple "prod info") rest $ \rs -> do
        [boxity, demands] <- pure rs
        b <- fromS boxity
        ds <- expectTuple "demands" (\dList -> mapM fromS dList) demands
        return $ Prod b ds

      _ -> fail "Invalid SubDemand type/Call is not exposed"

instance FromS Demand where
  fromS (SAtom (Atom dem)) = case dem of
    "botdmd" -> return BotDmd
    "absdmd" -> return AbsDmd
    _ -> fail "Invalid Demand"
  fromS _ = fail "Invalid Demand/D is not exposed"

instance FromS CardNonAbs where
  fromS (SAtom (Atom card)) = case card of
    "C_00" -> return C_00
    "C_0N" -> return C_0N
    "C_10" -> return C_10
    "C_1N" -> return C_1N
    _ -> fail "Invalid CardNonAbs"
  fromS _ = fail "Needs an atom for CardNonAbs"

instance FromS Boxity where
  fromS s = case s of
    (SAtom (Atom "boxed"))   -> return Boxed
    (SAtom (Atom "unboxed")) -> return Unboxed
    _ -> fail "Invalid Boxity"

instance FromS Divergence where
  fromS s = case s of
    (SAtom (Atom "diverges")) -> return Diverges
    (SAtom (Atom "exnordiv")) -> return ExnOrDiv
    (SAtom (Atom "dunno"))    -> return Dunno
    _ -> fail "Invalid Divergence"

instance FromS CprSig where
  fromS s = do
    cprType <- fromS s
    return $ CprSig cprType

instance FromS CprType where
  fromS s = flip (expectTuple "CprType info") s $ \xs -> do
    [SAtom (Atom "CprType"), SInt arity, cprInfo] <- pure xs

    cpr <- fromS cprInfo

    return $ CprType (fromInteger arity) cpr

instance FromS Cpr where
  fromS s = flip (expectTuple "Cpr info") s $ \xs -> do
    [SAtom (Atom cprType), rest] <- pure xs

    case cprType of
      "BotCpr" -> return botCpr
      "TopCpr" -> return topCpr

      "ConCpr" -> flip (expectTuple "ConCpr info") rest $ \rs -> do
        [SInt tag, cprs] <- pure rs
        cprList <- expectTuple "list of Cpr" (mapM fromS) cprs
        return $ ConCpr (fromInteger tag) cprList

      "FlatConCpr" -> case rest of
        SInt tag -> return $ flatConCpr (fromInteger tag)
        _ -> fail "Invalid ConCpr"

      _ -> fail "Invalid Cpr type"

instance FromS Activation where
  fromS s = flip (expectTuple "Activation info") s $ \xs -> do
    [SAtom (Atom activationType), rest] <- pure xs

    case activationType of
      "AlwaysActive" -> return AlwaysActive
      "FinalActive"  -> return FinalActive
      "NeverActive"  -> return NeverActive

      "ActiveBefore" -> flip (expectTuple "Active before") rest $ \rs -> do
        [SAtom (Atom srcText), SInt phase] <- pure rs
        return $ ActiveBefore (SourceText srcText) (fromInteger phase)

      "ActiveAfter" -> flip (expectTuple "Active after") rest $ \rs -> do
        [SAtom (Atom srcText), SInt phase] <- pure rs
        return $ ActiveAfter (SourceText srcText) (fromInteger phase)

      _ -> fail "Invalid Activation type"

instance FromS Type where
    fromS s = flip (expectTuple "Type info") s $ \xs -> do
        [SAtom (Atom "type"), rest'] <- pure xs
        flip (expectTuple "Type definition info") rest' $ \ls -> do
          [SAtom (Atom typeConstr), rest] <- pure ls
          case typeConstr of
            "TyVarTy" -> do
                v <- fromS rest
                return $ TyVarTy v
            "AppTy" -> flip (expectTuple "Type application info") rest $ \rs -> do
                [t1, t2] <- pure rs
                appt1 <- fromS t1
                appt2 <- fromS t2
                return $ AppTy appt1 appt2
            "TyConApp" -> flip (expectTuple "TyCon application info") rest $ \rs -> do
                tycon <- fromS (head rs)
                tys <- expectTuple "list of KindOrType" (mapM fromS) (rs !! 1)
                return $ TyConApp tycon tys
            "ForAllTy" -> flip (expectTuple "ForAllTy info") rest $ \rs -> do
                [t1, t2] <- pure rs
                binder <- fromS t1
                t <- fromS t2
                return $ ForAllTy binder t
            "FunTy" -> flip (expectTuple "FunTy info") rest $ \rs -> do
                [t1, t2, t3, t4] <- pure rs
                flag <- fromS t1
                mult <- fromS t2
                arg  <- fromS t3
                res  <- fromS t4
                return $ FunTy flag mult arg res
            "LitTy" -> do
                lit <- fromS rest
                return $ LitTy lit
            "CastTy" -> flip (expectTuple "CastTy info") rest $ \rs -> do
                [t1, t2] <- pure rs
                t <- fromS t1
                co <- fromS t2
                return $ CastTy t co
            "CoercionTy" -> do
                coercion <- fromS rest
                return $ CoercionTy coercion

            _ -> fail "Invalid Type information"

instance FromS DataCon where
    fromS s = flip (expectTuple "Datacon info") s $ \xs -> do
        [SAtom (Atom "data-con"), rest] <- pure xs
        flip (expectTuple "MkData info") rest $ \rs -> do
            dcName            <- fromS (rs !! 0)
            dcTag             <- fromS (rs !! 2)
            dcUnivTyVars      <- fromS (rs !! 4)
            dcExTyCoVars      <- fromS (rs !! 5)
            dcUserTyVarBinders<- fromS (rs !! 6)
            dcEqSpec          <- fromS (rs !! 7)
            dcOtherTheta      <- fromS (rs !! 8)
            dcStupidTheta     <- fromS (rs !! 9)
            dcOrigArgTys      <- fromS (rs !! 10)
            dcOrigResTy       <- fromS (rs !! 11)
            dcSrcBangs        <- fromS (rs !! 12)
            dcFields          <- fromS (rs !! 13)
            dcWorkId          <- fromS (rs !! 14)
            dcRep             <- fromS (rs !! 15)
            dcRepTyCon        <- fromS (rs !! 18)
            dcRepType         <- fromS (rs !! 19)
            dcInfix           <- fromS (rs !! 20)
            dcPromoted        <- fromS (rs !! 21)
            return $ mkDataCon dcName dcInfix dcPromoted dcSrcBangs dcFields dcUnivTyVars dcExTyCoVars dcUserTyVarBinders dcEqSpec dcOtherTheta dcOrigArgTys dcOrigResTy dcRepTyCon dcTag dcStupidTheta dcWorkId dcRep dcRepType

instance FromS Role where
    fromS s = case s of
        SAtom (Atom "Nominal")          -> return Nominal
        SAtom (Atom "Representational") -> return Representational
        SAtom (Atom "Phantom")          -> return Phantom
        _                               -> fail "Invalid Role representation"

instance FromS LeftOrRight where
    fromS s = case s of
        SAtom (Atom "CLeft") -> return CLeft
        SAtom (Atom "CRight") -> return CRight
        _ -> fail "Invalid LeftOrRight representation"

instance FromS FunSel where
    fromS s = case s of
        SAtom (Atom "SelMult") -> return SelMult
        SAtom (Atom "SelArg") -> return SelArg
        SAtom (Atom "SelRes") -> return SelRes
        _ -> fail "Invalid FunSel representation"

instance FromS Var where

instance FromS TyCon where

instance FromS ForAllTyBinder where

instance FromS FunTyFlag where
    fromS s = case s of
        SAtom (Atom "FTF_T_T")   -> return FTF_T_T
        SAtom (Atom "FTF_T_C")   -> return FTF_T_C
        SAtom (Atom "FTF_C_T")   -> return FTF_C_T
        SAtom (Atom "FTF_C_C")   -> return FTF_C_C
        _                        -> fail "Invalid FunTyFlag representation"

instance FromS TyLit where

instance FromS Coercion where
    fromS s = flip (expectTuple "Coercion info") s $ \xs -> do
        [SAtom (Atom "coercion"), rest'] <- pure xs
        flip (expectTuple "Coercion definition info") rest' $ \ls -> do
          [SAtom (Atom coe), rest] <- pure ls
          case coe of
            "Refl" -> do
              ty <- fromS s
              return $ Refl ty

            "GRefl" -> flip (expectTuple "GRefl info") rest $ \rs -> do
              [t1,t2,t3] <- pure rs
              role <- fromS t1
              ty <- fromS t2
              mco <- fromS t3
              return $ GRefl role ty mco

instance FromS MCoercionN where

instance FromS DataConRep where

instance FromS PromDataConInfo where

instance FromS FieldLabel where

instance FromS HsSrcBang where

instance FromS (Scaled Type) where

instance FromS EqSpec where

instance FromS (VarBndr TyVar Specificity) where

instance FromS Name where
