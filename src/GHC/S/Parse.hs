module GHC.S.Parse
  ( parseS
  ) where

import Text.Parsec
import Text.Parsec.String
import qualified Text.Parsec.Token as P
import Text.Parsec.Language (haskellDef)
import Data.Functor.Identity

import GHC.S.Types

lexer :: P.TokenParser ()
lexer = P.makeTokenParser haskellDef {
        P.reservedNames = []
    }

parseS :: String -> Either ParseError S
parseS = parse expr ""

sbinop :: Parser S
sbinop = P.parens lexer $ do
    e1 <- expr
    op <- P.operator lexer
    e2 <- expr
    return (STuple [SAtom (Atom op), e1, e2])

expr :: Parser S
expr = choice
    [ try sbinop
    , try (STuple <$> P.parens lexer (many1 expr))
    , SAtom  <$> (Atom <$> (P.identifier lexer <|> string "_"))
    , SString <$> P.stringLiteral lexer
    , SInt <$> P.integer lexer
    ]

