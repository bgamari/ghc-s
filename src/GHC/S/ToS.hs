module GHC.S.ToS
  ( ToS(..)
  , integral
  ) where

import GHC.S.Types

class ToS a where
  toS :: a -> S

integral :: Integral a => a -> S
integral = SInt . fromIntegral
