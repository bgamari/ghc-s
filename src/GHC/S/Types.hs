{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE GADTs #-}

module GHC.S.Types
  ( S(..)
  , Atom(..)
  , TheAtom(..)
  , SRepr
  , SBind(..)
  ) where

import GHC.TypeLits
import Data.String

type SRepr = [SBind]

data SBind
    = NonRecBind Atom S
    | RecBind [(Atom, S)] deriving Show

data S
  = STuple [S]
  | SAtom Atom
  | SInt Integer
  | SString String deriving Show

newtype Atom = Atom String
  deriving (Eq, Ord, Show, Read, IsString)

data TheAtom (s :: Symbol) where
    TheAtom :: KnownSymbol s => TheAtom s
